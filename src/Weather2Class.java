
public class Weather2Class {

	private double temp;
	private String city;
	
	public double getTemp() {
		return temp;
	}
	
	public void setTemp(double d) {
		this.temp=d;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city=city;
	}
	
}
