import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.xml.sax.helpers.DefaultHandler;
import javax.swing.JTextArea;
import org.json.*;
import java.sql.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tableDB;
	private JTable tableWeatherActual;
	private JTable tableWeatherTommorow;
	private JTextField textFieldCheckWeatherInTown;

	String weatherHTML = "empty";
	String parsedXml = "empty";

	Weather2Class userWeather;
	Weather2Class wroclawWeather;
	
	private static String weatherHtmlUrlCity;
	
	private static String weatherHtmlUrl = 
			"http://api.openweathermap.org/data/2.5/weather?q=Wroclaw&appid=cc68a686b603538e13a9e1477dc3821d";

	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	public String getHTML(String urlToRead) throws Exception {
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		for (String line = reader.readLine(); line != null; line = reader.readLine()) {
			result.append(line);
		}
		
		reader.close();
		return result.toString();
	}
	


	/**
	 * Create the frame.
	 */
	public MainFrame() {
		WeatherClass objectWeatherClass = new WeatherClass();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLoadFromDB = new JButton("Wczytaj z bazy danych");
		btnLoadFromDB.setBounds(32, 27, 209, 28);
		contentPane.add(btnLoadFromDB);
		
		JButton btnDeleteDB = new JButton("Usu\u0144 baz\u0119");
		btnDeleteDB.setBounds(276, 27, 93, 28);
		contentPane.add(btnDeleteDB);
		
		tableDB = new JTable();
		tableDB.setBounds(32, 105, 337, 175);
		contentPane.add(tableDB);
		
		JButton btnDeleteFromDB = new JButton("Usu\u0144 wybrane z bazy danych");
		btnDeleteFromDB.setBounds(32, 66, 209, 28);
		contentPane.add(btnDeleteFromDB);
		
		tableWeatherActual = new JTable();
		tableWeatherActual.setBounds(32, 346, 411, 42);
		contentPane.add(tableWeatherActual);
		
		JLabel lblAktualnaPogoda = new JLabel("Aktualna pogoda ( aktualizowane automatycznie )");
		lblAktualnaPogoda.setBounds(84, 321, 359, 14);
		contentPane.add(lblAktualnaPogoda);
		
		tableWeatherTommorow = new JTable();
		tableWeatherTommorow.setBounds(28, 427, 415, 42);
		contentPane.add(tableWeatherTommorow);
		
		JLabel lblPrognozaPogodyNa = new JLabel("Prognoza pogody na jutro ( aktualizowane automatycznie )");
		lblPrognozaPogodyNa.setBounds(61, 402, 382, 14);
		contentPane.add(lblPrognozaPogodyNa);
		
		textFieldCheckWeatherInTown = new JTextField();
		textFieldCheckWeatherInTown.setBounds(575, 54, 86, 20);
		contentPane.add(textFieldCheckWeatherInTown);
		textFieldCheckWeatherInTown.setColumns(10);
		
		JLabel lblSprawdzPogodeW = new JLabel("Sprawdz pogode w miescie:");
		lblSprawdzPogodeW.setBounds(550, 27, 159, 14);
		contentPane.add(lblSprawdzPogodeW);
		
		JLabel labelUserWeather = new JLabel("                -- -- --");
		labelUserWeather.setBounds(520, 164, 238, 28);
		contentPane.add(labelUserWeather);
		
		JLabel lblUserWeatherCity = new JLabel("Pogoda we --");
		lblUserWeatherCity.setBounds(550, 144, 359, 20);
		contentPane.add(lblUserWeatherCity);
		
		JLabel lblWeatherWroclaw = new JLabel("Pogoda we Wroclawiu");
		lblWeatherWroclaw.setBounds(539, 236, 359, 20);
		contentPane.add(lblWeatherWroclaw);
		
		JLabel labelWeatherWroclawShow = new JLabel("...");
		labelWeatherWroclawShow.setBounds(501, 251, 238, 42);
		contentPane.add(labelWeatherWroclawShow);
		
		JButton btnCheckWeatherInTown = new JButton("Wykonaj");
		btnCheckWeatherInTown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try 
				{
					lblUserWeatherCity.setText("Pogoda: " + textFieldCheckWeatherInTown.getText());

					weatherHtmlUrlCity="http://api.openweathermap.org/data/2.5/weather?q=" + textFieldCheckWeatherInTown.getText() + "&appid=cc68a686b603538e13a9e1477dc3821d";

					userWeather = getWeatherFrom(weatherHtmlUrlCity, textFieldCheckWeatherInTown.getText() );
					
					//lblUserWeatherCity.setText("Pogoda we " + userWeather.getCity());
					labelUserWeather.setText("Temperatura  wynosi " + userWeather.getTemp() + " C");
				}
				catch(Exception ex)
				{
					JFrame alert = new JFrame();
					JPanel mid = new JPanel();
					alert.getContentPane().add(mid, BorderLayout.PAGE_END);
					JLabel alertStatus = new JLabel("Nie wprowadzono nazwy!" , JLabel.CENTER);
					alert.setPreferredSize(new Dimension(200,100));
					alert.pack();
					alert.setResizable(false);
					alert.setLocationRelativeTo(null);
					alert.setVisible(true);
					alert.getContentPane().add(alertStatus);
					return;
						
				}
			
				
			}
		});
		
	
		btnCheckWeatherInTown.setBounds(559, 90, 120, 24);
		contentPane.add(btnCheckWeatherInTown);
	
	
		
		try {
			weatherHTML = getHTML(weatherHtmlUrl);

			JSONObject objectJSON = new JSONObject(weatherHTML); //tworze obiekt typu JSON i wprowadzam do niego stringa
			
			//String pressure = objectJSON.getJSONArray(key)objectJSON.getJSONObject("main").getString("pressure");
			double lon = objectJSON.getJSONObject("coord").getDouble("lon");
			int pressure = objectJSON.getJSONObject("main").getInt("pressure");
			double temp = objectJSON.getJSONObject("main").getDouble("temp") - 273.15;
			
			labelWeatherWroclawShow.setText("Cisnienie:" + pressure + " hPa, \r\n" + "Temperatura: " + temp + "C"); //testowa nazwa
			

		} catch (Exception e) {
			e.printStackTrace();
			labelWeatherWroclawShow.setText("Nieudane po��czenie !"); //testowa nazwa
			
		}
		

	}
	
	public Weather2Class getWeatherFrom(String URL, String city) {
		//String weatherHTML2;

		String weatherHTML2 = null;
		try {
			weatherHTML2 = getHTML(URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		
		Weather2Class object = new Weather2Class();
		JSONObject objectJSON;
		try {
			
		objectJSON = new JSONObject(weatherHTML2); //tworze obiekt typu JSON i wprowadzam do niego stringa
		object.setTemp(objectJSON.getJSONObject("main").getDouble("temp") - 273.15);
		object.setCity(city);
		}
		catch(Exception ex) {
			JFrame alert = new JFrame();
			JPanel mid = new JPanel();
			alert.getContentPane().add(mid, BorderLayout.PAGE_END);
			JLabel alertStatus = new JLabel("Wyst�pi� b��d!" + URL  , JLabel.CENTER);
			alert.setPreferredSize(new Dimension(200,100));
			alert.pack();
			alert.setResizable(true);
			alert.setLocationRelativeTo(null);
			alert.setVisible(true);
			alert.getContentPane().add(alertStatus);
		}
		return object;
	}
}
